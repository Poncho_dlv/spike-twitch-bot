[![DeepSource](https://deepsource.io/bb/Poncho_dlv/spike-twitch-bot.svg/?label=active+issues&show_trend=true&token=Ez0kZsJh-Qhigo0KpdgLMdO4)](https://deepsource.io/bb/Poncho_dlv/spike-twitch-bot/?ref=repository-badge)

# Quick Start

**Python 3.9 or higher**

You can clone Spike repo with your git client.

```ml
git clone https://Poncho_dlv@bitbucket.org/Poncho_dlv/spike-twitch-bot.git
```
or
```ml
git clone git@bitbucket.org:Poncho_dlv/spike-twitch-bot.git
```

Install requirement

```ml
pip install -r requirements.txt
```