import os
import json
from twitchio.ext import commands
from twitchio.ext.commands.core import Context

from Statistics import Statistics
from __init__ import spike_logger
from spike_settings.SpikeSettings import SpikeSettings

command_rate = 5


class TwitchBot(commands.Bot):
    def __init__(self):
        self.config = {}
        filename = os.environ.get("CONFIG_FILE", "config.json")
        try:
            with open(filename, encoding="utf-8") as f:
                json_data = json.load(f)
            f.close()
        except FileNotFoundError:
            json_data = {}

        channels = []
        for chanel in json_data.get("channels", []):
            self.config[chanel["name"]] = chanel
            channels.append(chanel["name"])

        super().__init__(token=SpikeSettings.get_twitch_token(), prefix="s!", initial_channels=channels)

    async def event_ready(self):
        spike_logger.info(f"Ready {self.nick}")

    async def check_right(self, ctx: Context):
        config = self.config.get(ctx.channel.name, None)
        if config is None:
            await ctx.send("Invalid configuration, you can post in #bug-report in spike discord.")
            return False

        if config["sub_only"] is False or ctx.author.is_subscriber is True:
            return True
        else:
            await ctx.send(f"{ctx.author.mention} this command is reserved to subscriber only.")
            return False

    def get_lang(self, ctx: Context):
        config = self.config.get(ctx.channel.name, None)
        return config["lang"]

    @commands.cooldown(rate=command_rate, per=60, bucket=commands.Bucket.user)
    @commands.command(name="vs")
    async def versus(self, ctx: commands.Context):
        if await self.check_right(ctx) is True:
            args = ctx.message.content.replace(ctx.prefix, "").replace("vs", "")
            output = Statistics.versus(args, lang=self.get_lang(ctx))
            await ctx.send(output)

    @commands.cooldown(rate=command_rate, per=60, bucket=commands.Bucket.user)
    @commands.command(name="wr")
    async def win_rate(self, ctx: commands.Context):
        if await self.check_right(ctx) is True:
            args = ctx.message.content.replace(ctx.prefix, "").replace("wr", "")
            output = Statistics.win_rate(args, lang=self.get_lang(ctx))
            await ctx.send(output)

    @commands.cooldown(rate=command_rate, per=60, bucket=commands.Bucket.user)
    @commands.command()
    async def cclwr(self, ctx: commands.Context):
        if await self.check_right(ctx) is True:
            args = ctx.message.content.replace(ctx.prefix, "").replace("cclwr", "")
            output = Statistics.win_rate(args, "ccl_win_rate", lang=self.get_lang(ctx))
            await ctx.send(output)

    @commands.cooldown(rate=command_rate, per=60, bucket=commands.Bucket.user)
    @commands.command(name="ccl")
    async def ccl_win_rate(self, ctx: commands.Context):
        if await self.check_right(ctx) is True:
            args = ctx.message.content.replace(ctx.prefix, "").replace("ccl", "")
            output = Statistics.win_rate(args, "ccl_win_rate", lang=self.get_lang(ctx))
            await ctx.send(output)

    @commands.cooldown(rate=command_rate, per=60, bucket=commands.Bucket.user)
    @commands.command(name="ccltop")
    async def ccl_top_race(self, ctx: commands.Context):
        if await self.check_right(ctx) is True:
            args = ctx.message.content.replace(ctx.prefix, "").replace("ccltop", "")
            output = Statistics.ccl_top_race(args, lang=self.get_lang(ctx))
            await ctx.send(output)

    @commands.cooldown(rate=command_rate, per=60, bucket=commands.Bucket.user)
    @commands.command(name="dode")
    async def dode(self, ctx: commands.Context):
        if await self.check_right(ctx) is True:
            args = ctx.message.content.replace(ctx.prefix, "").replace("dode", "")
            output = Statistics.compute_dode(args)
            await ctx.send(output)
