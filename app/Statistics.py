#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pyshorteners
from spike_requester.SpikeAPI.CclAPI import CclAPI
from spike_requester.SpikeAPI.StatisticsAPI import StatisticsAPI

from spike_database.BB2Resources import BB2Resources
from spike_database.Coaches import Coaches
from spike_database.ResourcesRequest import ResourcesRequest
from spike_translation import Translator
from spike_utilities.FuzzyUtils import FuzzyUtils
from spike_utilities.Utilities import Utilities


class Statistics:
    @staticmethod
    def versus(args: str, lang: str = "en"):
        common_db = ResourcesRequest()

        args = args.split(" ")
        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = "pc"
        else:
            # Remove platform from args if specified
            args.remove(platform)

        platform_id = common_db.get_platform_id(platform)
        platform_data = common_db.get_platform(platform_id)

        if len(args) > 0:
            args = " ".join(args)
            if "," not in args:
                args = args.strip()
                split_str = args.split(" ")
            else:
                split_str = args.split(",")
            if len(split_str) > 1:
                coach1 = Statistics.get_coach(split_str[0].strip(), platform_id)
                if coach1 is None:
                    return Translator.tr("#_exception.coach_not_found_platform", lang).format(split_str[0].strip(), platform)

                coach2 = Statistics.get_coach(split_str[1].strip(), platform_id)
                if coach2 is None:
                    return Translator.tr("#_exception.coach_not_found_platform", lang).format(split_str[1].strip(), platform)

                if coach1.get("id") is not None and coach2.get("id") is not None:
                    data = StatisticsAPI.get_versus(coach1.get("id"), coach2.get("id"), platform_id)

                    if data is not None:
                        win_coach1 = data.get("versus", {}).get("win_coach1")
                        draw = data.get("versus", {}).get("draw")
                        win_coach2 = data.get("versus", {}).get("win_coach2")

                        if None not in [win_coach1, draw, win_coach2]:
                            return f"[{platform_data[1]}] {coach1.get('name')} {win_coach1}-{draw}-{win_coach2} {coach2.get('name')}"
                        return Translator.tr("#_statistics_exception.no_games_found_2_coach", lang).format(coach1.get("name"), coach2.get("name"))
                    return Translator.tr("#_statistics_exception.no_games_found_2_coach", lang).format(coach1.get("name"), coach2.get("name"))
                if coach1 is None:
                    return Translator.tr("#_exception.coach_not_found", lang).format(args)
                if coach2 is None:
                    return Translator.tr("#_exception.coach_not_found", lang).format(args)
            else:
                # /* Give 2 valid coach name comma separated */
                return Translator.tr("#_statistics_command.give_2_coach_name_error", lang)

    @staticmethod
    def win_rate(args, league_name: str = None, lang: str = "en"):
        common_db = ResourcesRequest()

        args = args.split(" ")
        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = "pc"
        else:
            # Remove platform from args if specified
            args.remove(platform)
        args = " ".join(args)
        platform_id = common_db.get_platform_id(platform)
        rsrc_db = ResourcesRequest()

        if platform_id is not None:
            args = args.split(",")
            if len(args) > 1:
                bb_db = BB2Resources()
                race_label = args[1]
                args.remove(race_label)
                race_label = race_label.strip()
                race_id = rsrc_db.get_race_id_with_short_name(race_label.lower())
                if race_id is None:
                    race_id = bb_db.get_race_id_with_short_name(race_label.lower())

                    if race_id is None:
                        return Translator.tr("#_exception.invalid_race", lang).format(race_label=race_label)

                searched_coach = " ".join(args).strip()
                coach = Statistics.get_coach(searched_coach, platform_id)
                if coach is not None:
                    if league_name == "ccl_win_rate":
                        stats = CclAPI.get_win_rate(coach.get("id"), platform_id, race_id)
                        if stats.get('data') is not None:
                            stats = stats['data']
                    else:
                        stats = StatisticsAPI.get_win_rate(coach.get("id"), platform_id, race_id, league_name=league_name)
                    if stats is not None and stats.get("nb_match", 0) > 0:
                        matches_word = Translator.tr("#_statistics_command.matches_word", lang)
                        race_name = bb_db.get_race_label(race_id)
                        output = f"[{platform}] {race_name} {coach.get('name')}: {stats.get('win_rate')}% ({stats.get('nb_match')} {matches_word}) " \
                                 f"{stats.get('nb_win')}-{stats.get('nb_draw')}-{stats.get('nb_loss')}"
                        return output
                    return Translator.tr("#_statistics_exception.no_games_found_1_coach", lang).format(coach.get("name"))
                return Translator.tr("#_exception.coach_not_found", lang).format(searched_coach)
            top_race_limit = 3
            if league_name is None:
                top_league_limit = 3
            else:
                top_league_limit = 0

            searched_coach = " ".join(args).strip()
            coach = Statistics.get_coach(searched_coach, platform_id)
            if coach is not None:
                if league_name == "ccl_win_rate":
                    stats = CclAPI.get_win_rate(coach.get("id"), platform_id, top_race_limit=top_race_limit)
                    if stats.get('data') is not None:
                        stats = stats['data']
                else:
                    stats = StatisticsAPI.get_win_rate(coach.get("id"), platform_id, top_race_limit=top_race_limit,
                                                       top_league_limit=top_league_limit, league_name=league_name)
                if stats is not None and stats.get("nb_match", 0) > 0:
                    matches_word = Translator.tr("#_statistics_command.matches_word", lang)
                    output = f"[{platform}] {coach.get('name')}: {stats.get('win_rate')}% ({stats.get('nb_match')} {matches_word}) - " \
                             f"{stats.get('nb_win')}-{stats.get('nb_draw')}-{stats.get('nb_loss')}"

                    games_label = Translator.tr("#_common_command.games_label", lang)

                    # /* Top 3 race */
                    if len(stats.get("top_races", [])) > 0:
                        top_races_title = Translator.tr("#_statistics_command.top_race_title", lang)
                        output += f"  :: {top_races_title}:"
                        for idx, race in enumerate(stats.get("top_races", []), 1):
                            output += f" #{idx} {race.get('name')} {race.get('win_rate')}% ({race.get('nb_match')} {games_label}) " \
                                      f"{race.get('nb_win')}-{race.get('nb_draw')}-{race.get('nb_loss')}"

                    # /* Top 3 league */
                    if len(stats.get("top_leagues", [])) > 0:
                        top_leagues_title = Translator.tr("#_statistics_command.top_league_title", lang)
                        output += f"  :: {top_leagues_title}:"
                        for idx, league in enumerate(stats.get("top_leagues", []), 1):
                            output += f"  #{idx} {league.get('name')} {league.get('win_rate')}% ({league.get('nb_match')} {games_label}) " \
                                      f"{league.get('nb_win')}-{league.get('nb_draw')}-{league.get('nb_loss')}"

                    s = pyshorteners.Shortener()
                    url = s.isgd.short(f"https://spike.ovh/coach?coach_id={coach.get('id')}&platform_id={platform_id}")
                    output += f" :: {url}"

                    return output
                return Translator.tr("#_statistics_exception.no_games_found_1_coach", lang).format(coach.get("name"))
            return Translator.tr("#_exception.coach_not_found", lang).format(searched_coach)
        return Translator.tr("#_exception.invalid_platform", lang).format(platform=platform)

    @staticmethod
    def ccl_top_race(args, lang: str = "en"):
        common_db = ResourcesRequest()
        limit = 5

        args = args.split(" ")
        platform = Utilities.get_platform_in_args(args)
        if platform is None:
            platform = "pc"
        else:
            # Remove platform from args if specified
            args.remove(platform)

        platform_id = common_db.get_platform_id(platform)
        platform_data = common_db.get_platform(platform_id)
        rsrc_db = ResourcesRequest()
        bb_db = BB2Resources()

        if platform_id is not None:
            args = " ".join(args)
            race_label = args.strip()
            race_id = rsrc_db.get_race_id_with_short_name(race_label.lower())
            if race_id is None:
                race_id = bb_db.get_race_id_with_short_name(race_label.lower())

                if race_id is None:
                    return Translator.tr("#_exception.invalid_race", lang).format(race_label=race_label)

            race_label = bb_db.get_race_label(race_id)
            stats = CclAPI.get_top_race(platform_id, 53, race_label, limit)
            if stats is None or stats.get('data') is None or stats.get('data', {}).get('data'):
                return Translator.tr("#_exception.invalid_race", lang).format(race_label=race_label)

            stats = stats['data']

            if stats.get("races_rank"):
                output = f"[{platform_data[1]}] Top {limit} {race_label}: "
                for idx, team in enumerate(stats.get("races_rank", {}).get(race_label, []), 1):
                    wdl = f"{team.get('win')}-{team.get('draw')}-{team.get('loss')}"
                    if team.get("win_behind_first", 0) > 0:
                        template = " #{} {} {} ({}) {} WBF :: "
                        output += template.format(idx, team.get("coach_name"), team.get("sort"), wdl, team.get("win_behind_first"))
                    else:
                        template = " #{} {} {} ({}) :: "
                        output += template.format(idx, team.get("coach_name"), team.get("sort"), wdl)

                s = pyshorteners.Shortener()
                url = f"https://spike.ovh/competition_top_races?competition={stats.get('id')}&platform={platform_id}&race={race_label}&limit=50"
                url = s.isgd.short(url)
                output += f" Top 50: {url}"
                return output

    @staticmethod
    def get_coach(searched_name, platform_id):
        fuzzy = FuzzyUtils()
        coach_db = Coaches()
        coaches = coach_db.get_coaches_like(searched_name, platform_id, False)
        coaches_name = []
        for coach in coaches:
            coaches_name.append(coach.get("name"))
        ret = None
        if len(coaches_name) > 0:
            if len(coaches_name) == 1:
                ret = coaches_name[0]
            else:
                res = fuzzy.get_best_match_into_list(searched_name, coaches_name)
                if res is not None:
                    ret = res[0]
        if ret is not None:
            for coach in coaches:
                if ret == coach.get("name"):
                    return coach
        return ret

    @staticmethod
    def compute_dode(args):
        args = args.strip()
        if '-' in args:
            args = args.split("-")
        else:
            args = args.split(" ")

        if len(args) == 3:
            win = args[0]
            draw = args[1]
            loss = args[2]
        elif len(args) == 2:
            win = args[0]
            draw = args[1]
            loss = 0
        elif len(args) == 1:
            win = args[0]
            draw = 0
            loss = 0
        else:
            return 'Invalid syntax'

        data = StatisticsAPI.compute_dode(win, draw, loss)
        return data.get('msg')
