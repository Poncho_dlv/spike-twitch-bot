#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from TwitchBot import TwitchBot
from __init__ import spike_logger


def main():
    bot = TwitchBot()
    bot.run()


def except_hook(except_type, value, traceback):
    spike_logger.critical(except_type)
    spike_logger.critical(value)
    spike_logger.critical(traceback)


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
