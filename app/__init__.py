#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

spike_logger = logging.getLogger("spike_logger")
formatter = logging.Formatter(fmt="%(asctime)s :: %(levelname)s :: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z")
spike_logger.setLevel(logging.DEBUG)
spike_logger.addHandler(stream_handler)
