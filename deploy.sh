#!/bin/bash

git clone https://Poncho_dlv:"$1"@bitbucket.org/Poncho_dlv/"$2".git

# shellcheck disable=SC2164
cd "$2"

git submodule update --init --force --remote

docker-compose up --force-recreate --build -d

# shellcheck disable=SC2103
cd ..

# shellcheck disable=SC2115
rm -R "$2"/ -f